//SOAL N0 1 : Function Penghasil Tanggal Hari Esok
function next_date(tanggal,bulan,tahun){
	var date = new Date(tahun+'-'+bulan+'-'+tanggal);
	var nowDate = new Date(date.getTime() + 24 * 60 * 60 * 1000);
	var day = nowDate.getDate();
	var month = nowDate.getMonth() + 1;
	var year = nowDate.getFullYear();
	var bulan="";
	switch(month) {
	  case 1:   { bulan = 'Januari'; break; }
	  case 2:   { bulan = 'Februari'; break; }
	  case 3:   { bulan = 'Maret'; break; }
	  case 4:   { bulan = 'April'; break; }
	  case 5:   { bulan = 'Mei'; break; }
	  case 6:   { bulan = 'Juni'; break; }
	  case 7:   { bulan = 'Juli'; break; }
	  case 8:   { bulan = 'Agustus'; break; }
	  case 9:   { bulan = 'September'; break; }
	  case 10:   { bulan = 'Oktober'; break; }
	  case 11:   { bulan = 'November'; break; }
	  case 12:   { bulan = 'Desember'; break; }
	  default:  { bulan = 'undefined'; }}

	return day + " " + bulan + " " + year ;
}
console.log(next_date(29,2,2020));



//SOAL N0 2 : Function Penghitung Jumlah Kata
function jumlah_kata(kata) { 
    return kata.split(" ").length;
  }
  
  var kalimat_1 = "Halo nama saya Sayyid Ahsan Darojat";
  var kalimat_2 = "Saya Satpol";
  
  console.log(jumlah_kata(kalimat_1));
  console.log(jumlah_kata(kalimat_2));