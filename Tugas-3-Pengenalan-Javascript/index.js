//SOAL NO 1
var first = "saya sangat senang hari ini";
var second = "belajar javascript itu keren";
var result1 = first.substr(0, 19)
var result2 = second.substr(0, 7)
var result3 = second.substr(7, 11).toUpperCase()

console.log(result1 + result2 + result3); //... Jawaban soal 1


//SOAL NO 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var total1 = Number(kataPertama) + Number(kataKedua)
var total2 = Number(kataKeempat) - Number(kataKetiga)
var allTotal = Number(total1) * Number(total2)

console.log(allTotal); //... Jawaban soal 2


//SOAL NO 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18)
var kataKeempat = kalimat.substring(19, 24)
var kataKelima = kalimat.substring(25, 31)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima); //... Jawaban soal 3